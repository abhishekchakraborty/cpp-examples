//
// Created by abhishek on 24/5/16.
//

#include <chrono>
#include <thread>
#include "utils.h"

void
sleep(unsigned microSecs)
{
    std::this_thread::sleep_for(std::chrono::microseconds(microSecs));
}

void
suspendOStream(std::function<void(void)> func, std::ostream& stream)
{
    stream.setstate(std::ios::failbit);
    func();
    stream.clear();
}