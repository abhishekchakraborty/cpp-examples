//
// Created by abhishek on 6/2/17.
//

#ifndef CPPEXAMPLES_EXAMPLE14_H
#define CPPEXAMPLES_EXAMPLE14_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cmath>

// Reading Delimiter-Separated Values(DSV)
void readDSV(std::istream& input, std::vector<std::vector<std::string>>& output, char delimiter = ',')
{
    std::string dsvLine;

    while (std::getline(input, dsvLine)) {
        std::istringstream dsvStream(dsvLine);
        std::vector<std::string> dsvRow;
        std::string dsvElement;

        while (std::getline(dsvStream, dsvElement, delimiter)) {
            dsvRow.push_back(dsvElement);
        }

        output.push_back(dsvRow);
    }
}

// Writing Delimiter-Separated Values(DSV)
void writeDSV(std::vector<std::vector<std::string>>& input, std::ostream& output, char delimiter = ',')
{
    for (int i = 0; i < input.size(); ++i) {
        if (!input[i].empty()) {
            output << input[i][0];
            for (int j = 1; j < input[i].size(); ++j) {
                output << delimiter << input[i][j];
            }
        }
        output << "\n";
    }
}

// Generating DSV
void generateDSVContent(std::vector<std::vector<std::string>>& dsvContent, int rows, int maxRowCols, std::string commonValue = "")
{
    auto getUniform = [](){ long randVal = std::rand(); return (randVal/(1.0*RAND_MAX)); };
    auto getRandInt= [&getUniform](int lowerBound, int upperBound){
        return static_cast<int>(lowerBound + std::floor((upperBound - lowerBound)*getUniform()));
    };

    for (int i = 0; i < rows; ++i) {
        int numCols = getRandInt(1, maxRowCols);

        std::vector<std::string> dsvLine;
        dsvLine.reserve(numCols);

        for (int j = 0; j < numCols; ++j)
            dsvLine.push_back( ( !commonValue.compare("") ) ? std::to_string(getRandInt(0, numCols)) : commonValue );

        dsvContent.push_back(dsvLine);
    }

    return;
}

void example14()
{
    std::string fileName = R"(/home/abhishek/Desktop/tmp.dsv)";
    std::vector<std::vector<std::string>> dsvContent;
    char delimiter = '#';

    // Create DSV content
    generateDSVContent(dsvContent, 10, 10);

    // written content
    std::cout << "Written DSV content:\n";
    for(auto& line: dsvContent) {
        for(auto& elem: line)
            std::cout << elem << delimiter;
        std::cout << "\n";
    }
    std::cout << "\n";

    // write to DSV file
    std::fstream file(fileName, std::ios::out);
    if (!file.is_open()) {
        std::cout << "File could not be created!\n";
        return;
    }
    writeDSV(dsvContent, file, delimiter);
    file.close();
    dsvContent.clear();

    // Read from DSV file
    std::fstream file2(fileName, std::ios::in);
    if (!file2.is_open()) {
        std::cout << "Could not read from file!\n";
        return;
    }
    readDSV(file2, dsvContent, delimiter);
    file2.close();

    // print read content
    std::cout << "Read CSV content:\n";
    for(auto& line: dsvContent) {
        for(auto& elem: line)
            std::cout << elem << ",";
        std::cout << "\n";
    }

    return;
}

#endif //CPPEXAMPLES_EXAMPLE14_H
