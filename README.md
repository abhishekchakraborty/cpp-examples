# cpp-examples

## Introduction ##

This repository contains examples in C++11 ( and sometimes older versions ) and later versions.

The examples could serve one or more of following purposes:

* Important language feature
* Hard to understand Errors
* Unintuitive Behaviors

---

## Compiler ##

By default, [Clang](https://github.com/llvm-mirror/clang) is used. In order to use the default compiler available, comment following lines in `CMakeLists.txt` :

```cmake
UNSET( CLANG_PATH CACHE )
FIND_PROGRAM( CLANG_PATH NAMES clang++ )
SET( CMAKE_CXX_COMPILER "${CLANG_PATH}" )
```
---

## Build ##

The project requires [CMake](https://github.com/Kitware/CMake) to build. It can be built and run using following commands on Linux:
```shell
cmake .
make
./bin/CppExamples
```
---

