//
// Created by abhishek on 18/10/16.
//

#ifndef CPPEXAMPLES_EXAMPLE12_H
#define CPPEXAMPLES_EXAMPLE12_H

#include <cmath>
#include <vector>
#include <limits>
#include <algorithm>
#include <iostream>
#include "utils.h"

std::vector<float> divide(std::vector<float> const& dividend, std::vector<float> const& divisor)
{
    std::vector<float> quotient;
    quotient.reserve(4);
    for (int i = 0; i < 4; ++i) {
        std::cout << "dividend[" << i << "] = " << dividend[i] << "\n";
        std::cout << "divisor[" << i << "] = " << divisor[i] << "\n";
        if (std::abs(divisor[i]) < std::numeric_limits<float>::epsilon()) {
            if (std::abs(dividend[i]) < std::numeric_limits<float>::epsilon())
                quotient[i] = std::nanf("");
            else
                quotient[i] = std::numeric_limits<float>::infinity();
        } else {
            quotient[i] = dividend[i]/divisor[i];
        }
    }

    std::cout << "quotient is:\n";
    for (int j = 0; j < 4; ++j) {
        std::cout << quotient[j] << ",";
    }
    std::cout << "\n";

    return quotient;
}

std::vector<bool> checkNAN(std::vector<float> const& vec) {
    std::vector<bool> isNaN;
    std::transform(vec.begin(), vec.end(), isNaN.begin(), [](float f) { return std::isnan(f); } );
    return isNaN;
}

void example12()
{
    std::vector<float> v1 = {2.0f, 1.0f, 0.0f, 0.0f};
    std::vector<float> v2 = {2.0f, 0.0f, 1.0f, 0.0f};
    std::vector<float> numVec = divide(v1, v2);
    std::cout << "numVec computed\n";
    std::vector<bool> numVecNaN = checkNAN(numVec);
    std::cout << "numVecNaN computed\n";

    for (int i = 0; i < 4; ++i) {
        std::cout << v1[i] << "/" << v2[i] << " is NaN: " << (std::isnan(numVec[i]) ? "Y" : "N") << "\t";
        std::cout << v1[i] << "/" << v2[i] << " is Inf: " << (std::isinf(numVec[i]) ? "Y" : "N") << "\n";
    }
}

#endif //CPPEXAMPLES_EXAMPLE12_H
