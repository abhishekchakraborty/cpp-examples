//
// Created by abhishek on 7/10/16.
//

#ifndef CPPEXAMPLES_EXAMPLE10_H
#define CPPEXAMPLES_EXAMPLE10_H

#include <iostream>

class A {
public:
    void A1() {
        std::cout << "A1 called\n";
    }
    static void A2() {
        std::cout << "A2 called\n";
    }
};

namespace A {
    void A3() {
        std::cout << "A3 called\n";
    }
}

void example10()
{
    A a;
    a.A1();
    A::A2();
    A::A3();
}

#endif //CPPEXAMPLES_EXAMPLE10_H
