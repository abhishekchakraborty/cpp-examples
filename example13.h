//
// Created by abhishek on 3/12/16.
//

#ifndef CPPEXAMPLES_EXAMPLE13_H
#define CPPEXAMPLES_EXAMPLE13_H

#include <iostream>

int add(int x, int y) { return x + y; }
int sub(int x, int y) { return x - y; }
int mult(int x, int y) { return x * y; }
int divide(int x, int y) { return x / y; } // clashes with 'div' in stdlib

int (* getFunc(int funcIndex) )(int,int)
{
    int (*func)(int,int);
    switch (funcIndex) {
        case 0: func = &add; break;
        case 1: func = &sub; break;
        case 2: func = &mult; break;
        case 3: func = &divide; break;
        default: func = nullptr;
    }
    return func;
}

void example13()
{
    int funcIndex = -1;
    int x, y;

    std::cout << "Enter operation (0:Add, 1:Subtract, 2:Multiply, 3:Divide): ";
    std::cin >> funcIndex;

    std::cout << "1st integral operand: ";
    std::cin >> x;

    std::cout << "2nd integral operand: ";
    std::cin >> y;

    int (*fp)(int,int);
    fp = getFunc(funcIndex);

    // Equivalent invocations
    std::cout << "Operation Result 1 = " << (*fp)(x,y) << "\n";
    std::cout << "Operation Result 2 = " << fp(x,y) << "\n";
    std::cout << "Operation Result 3 = " << (**************fp)(x,y) << "\n"; // This is valid
    std::cout << "Operation Result 4 = " << getFunc(funcIndex)(x,y) << "\n";
}

#endif //CPPEXAMPLES_EXAMPLE13_H
