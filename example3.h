//
// Created by abhishek on 27/5/16.
//
#include <vector>
#include <iostream>
#include <iomanip>
#include <ostream>
#include <functional>
#include "utils.h"

template <typename X>
void moveAppend(std::vector<X>& src, std::vector<X>& dst)
{
    if (dst.empty())
    {
        dst = std::move(src);
    }
    else
    {
        dst.reserve(dst.size() + src.size());
        std::move(std::begin(src), std::end(src), std::back_inserter(dst));
        src.clear();
    }
}

int printIndex = 0;

class A {
public:
    A() { std::cout << std::setw(2) << printIndex++ << ":Default Constructor: i " << i << "\n" << std::flush; };
    ~A() { std::cout << std::setw(2) << printIndex++ << ":Default Destructor: i " << i << "\n" << std::flush; };
    A(int i2) { std::cout << std::setw(2) << printIndex++ << ":Alternate Constructor: i " << i << " => " << i2 << "\n" << std::flush; i = i2; };
    A(A const& a) { std::cout << std::setw(2) << printIndex++ << ":Copy Constructor: i " << i << " => " << a.i << "\n" << std::flush; i = a.i; };
    A(A&& a) { std::cout << std::setw(2) << printIndex++ << ":Move Constructor: i " << i << " => " << a.i << "\n" << std::flush; i = a.i; }
    A& operator=(A const& a) { std::cout << std::setw(2) << printIndex++ << ":Copy Assignment: i " << i << " => " << a.i << "\n" << std::flush; i = a.i; return *this; };
    A& operator=(A&& a) { std::cout << std::setw(2) << printIndex++ << ":Move Assignment: i " << i << " => " << a.i << "\n" << std::flush; i = a.i; return *this; };
private:
    int i = -1;
};

void clear(std::vector<A>& a1, std::vector<A>& a2)
{
    // clear
    a1.clear();
    a2.clear();
}

void initialize(std::vector<A>& a1, std::vector<A>& a2)
{
    // fill a1
    a1.push_back(A(1));
    a1.push_back(A(2));
    a1.push_back(A(3));
    a1.push_back(A(4));

    // fill a2
    a2.push_back(A(5));
    a2.push_back(A(6));
}

void example3()
{
    // Initialize
    std::vector<A> a1, a2;

    // Incrementally increase max-size of vectors
    std::cout << "\nAdjust Max Size of Vectors\n\n";
    suspendOStream( [&](){ clear(a1, a2); }, std::cout );
    initialize(a1, a2);

    // Initialize and Insert
    std::cout << "\nInitialize and Insert\n";

    // Output Initialization
    std::cout << "\nRun 1-1: Output Initialization\n\n";
    suspendOStream( [&](){ clear(a1, a2); }, std::cout );
    printIndex = 0;
    initialize(a1, a2);
    suspendOStream( [&](){ a1.insert(a1.end(), a2.begin(), a2.end()); }, std::cout );

    // Output Insertion
    std::cout << "\nRun 1-2: Output Insertion\n\n";
    suspendOStream( [&](){ clear(a1, a2); }, std::cout );
    printIndex = 0;
    suspendOStream( [&](){ initialize(a1, a2); }, std::cout );
    a1.insert(a1.end(), a2.begin(), a2.end());

    // Initialize and Move Append
    std::cout << "\nInitialize and Move Append\n";

    // Output Initialization
    std::cout << "\nRun 2-1: Output Initialization\n\n";
    suspendOStream( [&](){ clear(a1, a2); }, std::cout );
    printIndex = 0;
    initialize(a1, a2);
    suspendOStream( [&](){ moveAppend(a2, a1); }, std::cout );

    // Output Move Append
    std::cout << "\nRun 2-2: Output Move Append\n\n";
    suspendOStream( [&](){ clear(a1, a2); }, std::cout );
    printIndex = 0;
    suspendOStream( [&](){ initialize(a1, a2); }, std::cout );
    moveAppend(a2, a1);

    std::cout << "\nEnd\n\n";
};