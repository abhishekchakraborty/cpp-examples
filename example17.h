//
// Created by abhishek on 28/11/19.
//

#ifndef CPPEXAMPLES_EXAMPLE17_H
#define CPPEXAMPLES_EXAMPLE17_H

#include <iostream>

void example17()
{
    struct A { };
    struct B { A a1; A a2; };
    struct C { B b1; B b2; };
    
    std::cout << "sizeof(A) = " << sizeof(A) << std::endl;
    std::cout << "sizeof(B) = " << sizeof(B) << std::endl;
    std::cout << "sizeof(C) = " << sizeof(C) << std::endl;
}

#endif //CPPEXAMPLES_EXAMPLE17_H