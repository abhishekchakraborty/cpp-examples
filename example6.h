//
// Created by abhishek on 4/6/16.
//

#ifndef CPPEXAMPLES_EXAMPLE6_H
#define CPPEXAMPLES_EXAMPLE6_H

#include <bitset>
#include <vector>
#include <iostream>
#include "utils.h"

void example6()
{
    const int strLen = 10;
    std::string bitString = std::string(strLen,'1');

    std::cout << "bitString: " << bitString << std::endl;

    // bit-field type
    std::bitset<strLen> b1(bitString);
    std::vector<bool> b2(strLen,true);
    std::vector<char> b3(strLen,'1');

    std::cout << "\nValues are:" << std::endl;
    std::cout << "b1: " << b1 << std::endl;
    std::cout << "b2: " << vec_to_string(b2) << std::endl;
    std::cout << "b3: " << vec_to_string(b3) << std::endl;

    std::cout << "\nSizes are:" << std::endl;
    std::cout << "sizeof(b1): " << sizeof(b1) << std::endl;
    std::cout << "b1.size(): " << b1.size() << std::endl;
    std::cout << "sizeof(b2): " << sizeof(b2) << std::endl;
    std::cout << "sizeof(b3): " << sizeof(b3) << std::endl;
}

#endif //CPPEXAMPLES_EXAMPLE6_H
