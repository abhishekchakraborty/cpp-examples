//
// Created by abhishek on 5/3/17.
//

#ifndef CPPEXAMPLES_EXAMPLE16_H
#define CPPEXAMPLES_EXAMPLE16_H

// Source: http://blog.tartanllama.xyz/c++/2017/01/20/initialization-is-bonkers/

#include <iostream>

struct foo {
    foo() = default;
    int a;
};

struct bar {
    bar();
    int b;
};

bar::bar() = default;

void example16() {
    foo a{};
    bar b{};
    std::cout << a.a << ' ' << b.b;
}

#endif //CPPEXAMPLES_EXAMPLE16_H
