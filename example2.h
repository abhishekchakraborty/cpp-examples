//
// Created by abhishek on 25/5/16.
//

#ifndef CPPEXAMPLES_EXAMPLE2_H
#define CPPEXAMPLES_EXAMPLE2_H

#include <vector>
#include <iostream>
#include "utils.h"

class TrivialClass {
public:
    TrivialClass(const std::string& data) :
            mData(data) {};

    const std::string& getData(const TrivialClass& rhs) const {
        return rhs.mData;
    };

private:
    std::string mData;
};

void example2()
{
    TrivialClass a("fish");
    TrivialClass b("heads");

    std::cout << "b via a = " << a.getData(b) << std::endl;
    return;
}

#endif //CPPEXAMPLES_EXAMPLE2_H
