//
// Created by abhishek on 15/6/16.
//

#ifndef CPPEXAMPLES_EXAMPLE8_H
#define CPPEXAMPLES_EXAMPLE8_H

#include <random>
#include <iostream>
#include <bits/unique_ptr.h>

// Random Number Generator
class UniformRnd {
public:
    UniformRnd(double lowerLimit, double upperLimit): distr(lowerLimit, upperLimit) {
        std::random_device rd;
        std::mt19937 gen(rd());
        generator = gen;
    }
    double rand() {
        return distr(generator);
    }
    double lowerLimit() {
        return distr.min();
    }
    double upperLimit() {
        return distr.max();
    }
private:
    std::uniform_real_distribution<double> distr;
    std::mt19937 generator;
};

void generateRand(UniformRnd& uniformRnd)
{
    std::cout << "Random No. = " << uniformRnd.rand() << std::endl;
}

std::unique_ptr<UniformRnd> initialize(double lowerLimit, double upperLimit)
{
    auto rndGenerator = std::unique_ptr<UniformRnd>(new UniformRnd(lowerLimit, upperLimit));
    std::cout << "Random generator created for limits (" << rndGenerator->lowerLimit() << ", " << rndGenerator->upperLimit() << ")" << std::endl;
    return rndGenerator;
}

void example8()
{
    // Random No.s in (0,10)
    auto rand1 = initialize(0, 10);
    for (int i = 0; i < 10; ++i) {
        generateRand(*rand1);
    }

    // Random No.s in (-5,5)
    auto rand2 = initialize(-5, 5);
    for (int i = 0; i < 10; ++i) {
        generateRand(*rand2);
    }
}

#endif //CPPEXAMPLES_EXAMPLE8_H
