//
// Created by abhishek on 24/5/16.
//

#ifndef CPPEXAMPLES_EXAMPLE1_H
#define CPPEXAMPLES_EXAMPLE1_H

#include <vector>
#include <algorithm>
#include "utils.h"

void example1()
{
    std::vector<int> vertices = { 0, 1, 2 };
    std::cout << "vertices: " << vec_to_string(vertices) << std::endl;

    std::vector<std::string> verticesStr;
    std::transform(vertices.begin(), vertices.end(), verticesStr.begin(), [](int i){ return std::to_string(i); });
    std::cout << "verticesStr: " << vec_to_string(verticesStr) << std::endl;

    return;
}

#endif //CPPEXAMPLES_EXAMPLE1_H
