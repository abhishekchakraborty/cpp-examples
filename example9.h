//
// Created by abhishek on 3/9/16.
//

#ifndef CPPEXAMPLES_EXAMPLE9_H
#define CPPEXAMPLES_EXAMPLE9_H

#include <iostream>
using namespace std;

class Date
{
    int mo, da, yr;
public:
    Date(int m, int d, int y)
    {
        mo = m; da = d; yr = y;
    }
    friend ostream& operator<<(ostream& os, const Date& dt);
};

ostream& operator<<(ostream& os, const Date& dt)
{
    os << dt.mo << '/' << dt.da << '/' << dt.yr;
    return os;
}

void example9()
{
    Date dt(5, 6, 92);
    cout << dt;
}

#endif //CPPEXAMPLES_EXAMPLE9_H
