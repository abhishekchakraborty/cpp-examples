//
// Created by abhishek on 11/6/16.
//

#ifndef CPPEXAMPLES_EXAMPLE7_H
#define CPPEXAMPLES_EXAMPLE7_H

#include <random>
#include <iostream>

class UniformRnd {
private:
    static std::uniform_real_distribution<double> distr;
    static std::mt19937 generator;

public:
    static void init(double lowerLimit, double upperLimit) {
        // initialize using copy
        std::uniform_real_distribution<double> distr2(lowerLimit, upperLimit);
        distr = distr2;

        std::random_device rd;
        std::mt19937 gen(rd());
        generator = gen;
    }
    static double rand() {
        return distr(generator);
    }
    static double lowerLimit() {
        return distr.min();
    }
    static double upperLimit() {
        return distr.max();
    }
};

// Static Definitions
std::uniform_real_distribution<double> UniformRnd::distr;
std::mt19937 UniformRnd::generator;

void generateRand()
{
    std::cout << "Random No. = " << UniformRnd::rand() << std::endl;
}

void initialize(double lowerLimit, double upperLimit)
{
    UniformRnd::init(lowerLimit, upperLimit);
    std::cout << "Initialization done to limits (" << UniformRnd::lowerLimit() << ", " << UniformRnd::upperLimit() << ")" << std::endl;
}

void example7()
{
    // Random No.s in (0,10)
    initialize(0, 10);
    for (int i = 0; i < 10; ++i) {
        generateRand();
    }

    // Random No.s in (-5,5)
    initialize(-5, 5);
    for (int i = 0; i < 10; ++i) {
        generateRand();
    }
}



#endif //CPPEXAMPLES_EXAMPLE7_H
