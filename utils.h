//
// Created by abhishek on 24/5/16.
//

#ifndef CPPEXAMPLES_UTILS_H
#define CPPEXAMPLES_UTILS_H

#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <iterator>
#include <functional>

template <typename T>
std::string vec_to_string( std::vector<T> v )
{
    std::ostringstream ss;

    if (!v.empty()) {
        std::copy(v.begin(), v.end()-1, std::ostream_iterator<T>(ss, ",") );
        ss << v.back();
    }

    return ss.str();
}

template <typename T>
std::string list_to_string( std::list<T> l )
{
    std::ostringstream ss;

    if (!l.empty()) {
        auto end = l.begin();
        std::advance(end, l.size()-1);
        std::copy(l.begin(), end, std::ostream_iterator<T>(ss, ",") );
        ss << l.back();
    }

    return ss.str();
}

template <typename Iter>
std::string iter_to_string(Iter begin, Iter end, std::string sep=",")
{
    // Note: It is assumed that:
    // 1. 'begin' and 'end' point to same container
    // 2. 'begin' occurs before 'end'
    std::ostringstream oss;

    if (begin != end) {
        auto it = begin;
        oss << *it++;
        while (it != end) {
            oss << sep << *it++;
        }
    }

    return oss.str();
}

template <typename T>
void moveAppend(std::vector<T>& src, std::vector<T>& dst)
{
    if (dst.empty())
        dst = std::move(src);
    else {
        dst.reserve(dst.size() + src.size());
        std::move(std::begin(src), std::end(src), std::back_inserter(dst));
        src.clear();
    }
}

/** To perform time delay of given micro seconds */
void sleep(unsigned microSecs);

/** To execute given statement but discard the contents of the stream */
void suspendOStream(std::function<void(void)> func, std::ostream& stream);

#endif //CPPEXAMPLES_UTILS_H
