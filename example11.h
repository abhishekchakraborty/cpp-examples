//
// Created by abhishek on 13/10/16.
//

#ifndef CPPEXAMPLES_EXAMPLE11_H
#define CPPEXAMPLES_EXAMPLE11_H

#include <memory>
#include <iostream>

// Dummy class
class A {
private:
    int a;
public:
    A(int a1): a(a1) { }
};

void example11()
{
    std::unique_ptr<A> u_ptr = std::unique_ptr<A>(new A(1));

    std::cout << "Initialization with std::unique ptr and std::move\n";
    std::shared_ptr<A> s_ptr1(std::move(u_ptr));
//    std::shared_ptr<A> s_ptr3(u_ptr); // throws compilation error

    std::cout << "Assignment with std::unique ptr and std::move\n";
    std::shared_ptr<A> s_ptr2 = std::move(u_ptr);
//    std::shared_ptr<A> s_ptr4 = u_ptr; // throws compilation error
}

#endif //CPPEXAMPLES_EXAMPLE11_H