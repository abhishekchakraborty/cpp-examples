//
// Created by abhishek on 15/2/17.
//

#ifndef CPPEXAMPLES_EXAMPLE15_H
#define CPPEXAMPLES_EXAMPLE15_H

#include <iostream>
#include <memory>

class A {
private:
    std::string className = "A";
public:
    A() { std::cout << className << " object created\n"; }
    ~A() { std::cout << className << " object destroyed\n"; }
    virtual void f() { std::cout << className << "->f()\n"; }
    virtual void g() { std::cout << className << "->g()\n"; }
};

class B: public A {
private:
    std::string className = "B";
public:
    B() { std::cout << className << " created\n"; }
    ~B() { std::cout << className << " destroyed\n"; }
    void g() { std::cout << className << "->g()\n"; }
};

template <typename T>
void createAndInvoke()
{
    std::shared_ptr<A> a = std::make_shared<T>();
    a->f();
    a->g();
}

void example15()
{
    createAndInvoke<A>();
    createAndInvoke<B>();
}

#endif //CPPEXAMPLES_EXAMPLE15_H
