//
// Created by abhishek on 30/5/16.
//

#ifndef CPPEXAMPLES_EXAMPLE5_H
#define CPPEXAMPLES_EXAMPLE5_H

#include <iostream>
#include <vector>
#include <iterator>
#include "utils.h"

//template <typename Iterable>
//struct IterData {
//    std::size_t size;
//    typename Iterable::iterator begin;
//
//    IterData(Iterable vec) {
//        size = vec.size();
//        begin = vec.begin();
//    }
//};

//template <typename T>
//struct IterData {
//    std::size_t size;
//    typename std::iterator<std::bidirectional_iterator_tag,T> begin;
//
//    IterData(std::vector<T>& vec) {
//        size = vec.size();
//        begin = vec.begin();
//    }
//
//    IterData(std::list<T>& lst) {
//        size = lst.size();
//        begin = lst.begin();
//    }
//};

template <typename T>
struct VecData {
    std::size_t size;
    typename std::vector<T>::iterator begin;

    VecData(std::vector<T>& vec) {
        size = vec.size();
        begin = vec.begin();
    }
};

template <typename T>
struct ListData {
    std::size_t size;
    typename std::list<T>::iterator begin;

    ListData(std::list<T>& lst) {
        size = lst.size();
        begin = lst.begin();
    }
};

void example5()
{
    // initialize vectors
    std::vector<int> v1{1, 2, 3, 4};
    std::vector<int> v2{5, 6};

    std::cout << "\nVector Iterator member of Structs:\n" << std::endl;

    std::cout << "v1: " << vec_to_string(v1) << std::endl;
    std::cout << "v2: " << vec_to_string(v2) << std::endl;

    // Get Data
    VecData<int> vd1(v1), vd2(v2);

    std::cout << "Struct member pointers independently:" << std::endl;
    std::cout << "From id1.begin to v1.end: " << iter_to_string(vd1.begin, v1.end()) << std::endl;
    std::cout << "From id2.begin to v2.end: " << iter_to_string(vd2.begin, v2.end()) << std::endl;

    // List Container
    std::list<VecData<int>> vecDataList{vd1, vd2};

    std::cout << "Struct member pointers as elements of list:" << std::endl;
    std::cout << "From vecDataList.begin()->begin to v1.end: " << iter_to_string(vecDataList.begin()->begin, v1.end()) << std::endl;
    std::cout << "From vecDataList.rbegin()->begin to v2.end: " << iter_to_string(vecDataList.rbegin()->begin, v2.end()) << std::endl;

    // Vector Container
    std::vector<VecData<int>> vecDataVec{vd1, vd2};

    VecData<int> vd1Copy = vecDataVec[0];
    VecData<int> vd2Copy = vecDataVec[1];

    std::cout << "member pointers of Structs copied from vector elements:" << std::endl;
    std::cout << "From v3.begin to v1.end: " << iter_to_string(vd1Copy.begin, v1.end()) << std::endl;
    std::cout << "From v4.begin to v2.end: " << iter_to_string(vd2Copy.begin, v2.end()) << std::endl;

    // Merge
    std::vector<int> v3;
    moveAppend(v1, v3);
    moveAppend(v2, v3);

    std::cout << "Merged Vector" << std::endl;
    std::cout << "v3: " << vec_to_string(v3) << std::endl;

    std::cout << "Struct member pointers as elements of vector:" << std::endl;
    std::cout << "From vecDataVec[0].begin to v3.end: " << iter_to_string(vecDataVec[0].begin, v3.end()) << std::endl;
    std::cout << "From vecDataVec[1].begin to v3.end: " << iter_to_string(vecDataVec[1].begin, v3.end()) << std::endl;
    std::cout << "From vecDataVec[0].begin to vecDataVec[1].begin: " << iter_to_string(vecDataVec[0].begin, vecDataVec[1].begin) << std::endl;



    // initialize lists
    std::list<int> l1{11, 12, 13, 14};
    std::list<int> l2{15, 16};

    std::cout << "\nList Iterator member of Structs:\n" << std::endl;

    std::cout << "l1: " << list_to_string(l1) << std::endl;
    std::cout << "l2: " << list_to_string(l2) << std::endl;

    // Get Data
    ListData<int> ld1(l1), ld2(l2);

    std::cout << "Struct member pointers independently:" << std::endl;
    std::cout << "From ld1.begin to l1.end: " << iter_to_string(ld1.begin, l1.end()) << std::endl;
    std::cout << "From ld2.begin to l2.end: " << iter_to_string(ld2.begin, l2.end()) << std::endl;

    // Merge
    std::list<int> l3;
    l3.splice(l3.end(), l1);
    l3.splice(l3.end(), l2);

    std::cout << "Merged List" << std::endl;
    std::cout << "l3: " << list_to_string(l3) << std::endl;

    // Vector Container
    std::vector<ListData<int>> lstDataVec{ld1, ld2};
    std::cout << "member pointers of Structs copied from vector elements:" << std::endl;
    std::cout << "From lstDataVec[0].begin to l3.end: " << iter_to_string(lstDataVec[0].begin, l3.end()) << std::endl;
    std::cout << "From lstDataVec[1].begin to l3.end: " << iter_to_string(lstDataVec[1].begin, l3.end()) << std::endl;
    std::cout << "From lstDataVec[0].begin to lstDataVec[1].begin: " << iter_to_string(lstDataVec[0].begin, lstDataVec[1].begin) << std::endl;
}

#endif //CPPEXAMPLES_EXAMPLE5_H
