//
// Created by abhishek on 30/5/16.
//

#ifndef CPPEXAMPLES_EXAMPLE4_H
#define CPPEXAMPLES_EXAMPLE4_H

#include <list>
#include <iostream>
#include "utils.h"

void print_lists(std::list<int>& l1, std::list<int>& l2)
{
    std::cout << "l1 = " << list_to_string(l1) << std::endl;
    std::cout << "l2 = " << list_to_string(l2) << std::endl;
    std::cout << std::endl;
}

void example4()
{
    std::list<int> l1{1, 2, 3, 4};
    std::list<int> l2{5, 6};
    print_lists(l1, l2);

    // Append at end
    l1.splice(l1.end(), l2);
    print_lists(l1, l2);

    // Append at front
    l2.splice(l2.begin(), l1);
    print_lists(l1, l2);

    // Append in middle
    auto middle = l2.begin();
    std::advance(middle, 4);
    l1.splice(l1.begin(), l2, middle, l2.end());
    print_lists(l1, l2);
}

#endif //CPPEXAMPLES_EXAMPLE4_H
